import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

/******
  To use lazy laoding
  change component loading from
    require('...')
  to
    () => import('...')
*****/

Vue.use(Router)

var router =  new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'root',
      redirect: '/dashboard'
    },
    {
      path: '/demo/image-search',
      name: 'similarImageSearch',
      component: require('@/components/demo/similarImageSearch/similarImageSearch.vue')
    },
    {
      path: '/demo/video-search',
      name:'similarVideoSearch',
      component: require('@/components/demo/videoDemo/videoDemo.vue')
    },
    {
      path: '/dashboard',
      component: require('@/components/dashboard/dashboard.vue'),
      redirect: '/dashboard/products',
      meta: { requiresAuth: true },
      children: [
        {
          path: 'products',
          component: require('@/components/dashboard/userProducts/userProducts.vue'),
          redirect: '/dashboard/products/ScopeCheck',
          children: [
            {
              path: 'ScopeCheck',
              component: require('@/components/dashboard/userProducts/scopeCheck/scopeCheck.vue'),
              redirect: '/dashboard/products/ScopeCheck/usage',
              children: [
                {
                  path: 'usage',
                  component: require('@/components/dashboard/userProducts/scopeCheck/productUsage/productUsage.vue')
                },
                {
                  path: 'credentials',
                  component: require('@/components/dashboard/userProducts/scopeCheck/credentials/credentials.vue')
                },
                {
                  path: 'similar-search',
                  component: require('@/components/dashboard/userProducts/scopeCheck/similarSearch/similarSearch.vue')
                },
                {
                  path: 'similar-search/:applicationId',
                  component: require('@/components/dashboard/userProducts/scopeCheck/similarSearch/similarSearch.vue'),
                  props: true
                }
              ]
            },
            {
              path: 'demo',
              component: require('@/components/dashboard/userProducts/dashboardDemo/dashboardDemo.vue'),
              redirect: '/dashboard/products/demo/auto-tagging',
              meta: { adminOnly: true },
              children: [
                /* Fashion */
                {
                  path: 'AI-designer',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/AIDesigner/AIDesigner.vue')
                },
                {
                  path: 'AI-designer-2',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/AIDesigner2.vue')
                },
                {
                  path: 'AI-designer-3',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/AIDesigner3.vue')
                },
                {
                  path: 'fashion-score',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/fashionScore/fashionScore.vue')
                },
                {
                  path: 'auto-tagging',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/autoTagging.vue')
                },
                {
                  path: 'fashion-style',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/fashionStyle.vue')
                },
                /* Video */
                {
                  path: 'video-map',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/videoMap.vue')
                },
                {
                  path: 'tagged-video-map',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/taggedVideoMap.vue')
                },
                {
                  path: 'video-highlight',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/videoHighlight/videoHighlight.vue')
                },
                {
                  path: 'video-ad',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/videoAd/videoAd.vue')
                },
                {
                  path: 'video-search',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/videoSearch/videoSearch.vue')
                },
                /* Others */
                {
                  path: 'batch-tagged-sample',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/batchTagged/batchTagged.vue')
                },
                {
                  path: 'medical-tagging',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/medicalTagging.vue')
                },
                {
                  path: 'floor-similar-search',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/floorSimilarSearch.vue')
                },
                {
                  path: 'sentence-to-image-search',
                  component: require('@/components/dashboard/userProducts/dashboardDemo/sentenceToImageSearch.vue')
                }
              ]
            }
          ]
        },
        {
          path: 'products/ScopeVision',
          component: require('@/components/dashboard/scopeVision/scopeVision.vue'),
          meta: { adminOnly: true },
          children: [
            {
              path: '/',
              component: require('@/components/dashboard/scopeVision/home.vue')
            },
            {
              path: 'admin',
              component: require('@/components/dashboard/scopeVision/admin.vue')
            },
            {
              path: 'admin/trend/:trendingId',
              component: require('@/components/dashboard/scopeVision/adminTrend.vue'),
              props: true
            },
            {
              path: ':trendingId',
              component: require('@/components/dashboard/scopeVision/trending.vue'),
              props: true
            },
            {
              path: ':trendingId/source',
              component: require('@/components/dashboard/scopeVision/source.vue'),
              props: true
            },
            {
              path: ':trendingId/graph/:graphId',
              component: require('@/components/dashboard/scopeVision/graph.vue'),
              props: true
            },
            {
              path: ':trendingId/trends',
              component: require('@/components/dashboard/scopeVision/trends.vue'),
              props: true
            },
            {
              path: ':trendingId/trends/edit',
              component: require('@/components/dashboard/scopeVision/trends.vue'),
              props: true
            },
            {
              path: ':trendingId/recommendation',
              component: require('@/components/dashboard/scopeVision/recommendation.vue'),
              props: true
            },
            {
              path: ':trendingId/matching',
              component: require('@/components/dashboard/scopeVision/matching.vue'),
              props: true
            }
          ]
        },
        {
          path: 'profile',
          name: 'userProfile',
          component: require('@/components/dashboard/userProfile/userProfile.vue')
        },
      ]
    },
    {
      path: '/confirm',
      name: 'confirm',
      component: require('@/components/account/confirmPage/confirm.vue'),
    },
    {
      path: '/resetpw',
      name: 'resetpw',
      component: require('@/components/account/resetPassword/resetPassword.vue'),
    },
    {
      path:'/login',
      name: 'login',
      component: require('@/components/account/login/login.vue'),
      meta: { redirectIfLoggedIn: true }
    },
    {
      path: '/signup',
      name: 'signup',
      component: require('@/components/account/signup/signup.vue'),
      meta: { redirectIfLoggedIn: true }
    },
    {
      path: '*',
      name: 'pageNotFound',
      component: require('@/components/pageNotFound/pageNotFound.vue')
    }
  ]
})

router.beforeEach(async (to, from, next) => {
  window.scrollTo(0, 0)

  // access token found but no user info. it may because its the first time user open this app so user info hasn't been retrived and recorded
  if (store.getters.hasAccessToken && !store.getters.hasUserInfo) {
    // try to get user info first
    await store.dispatch('getAndSetUserInfo').catch((error) => { console.error(error) })
  }
  
  const loggedIn = store.getters.hasUserInfo
  if (loggedIn && to.matched.some(record => record.meta.redirectIfLoggedIn)) { // if logged in and page is login, signup...etc, redirect user to dashboard
    next({path: '/dashboard/products/ScopeCheck'})
  } else if (loggedIn && !store.getters.isAdmin && to.matched.some(record => record.meta.adminOnly)) { // redirect non admin user
    next({path: '/dashboard/products/ScopeCheck'})
  } else if (!loggedIn && to.matched.some(record => record.meta.requiresAuth)) { // if not logged in and page requires login, redirect user to login page
    next({path: '/login',query: {redirect: to.fullPath}})
  } else {
    next() // nothing special...just next
  }
})

export default router
