const EXIF = require('exif-js')
const Utils = {
  /*
  * given image src, check orientation (orientation detail: http://jpegclub.org/exif_orientation.html)
  * return Promise resolve {image: Image object, orientation: orientation value}
  */
  getImageOrientation (imgSrc) {
    return new Promise((resolve, reject) => {
      var image = new Image()
      image.onerror = image.onabort = (e) => {
        reject('error loading image in getImageOrientation')
      }
      image.onload = () => {
        try {
          // cross test
          var canvas = document.createElement('canvas')
          var ctx = canvas.getContext('2d')
          ctx.drawImage(image, 0, 0)
          ctx.getImageData(0, 0, 1, 1)

          EXIF.getData(image, () => {
            resolve({
              image: image,
              orientation: EXIF.getTag(image, 'Orientation') || 0
            })
          })
        } catch (e) {
          resolve({
            image: image,
            orientation: 0
          })
        }
      }
      image.src = imgSrc
      image.crossOrigin = 'Anonymous'
    })
  },
  /*
  * given image, check orientation and apply fixes accordingly
  * return Promise resolve {image: fixed Image/Canvas object, src: fixed src}
  */
  fixImageOrientation (imgSrc) {
    return new Promise((resolve, reject) => {
      this.getImageOrientation(imgSrc).then(({image, orientation}) => {
        if (orientation <= 1) {
          resolve({image: image, src: imgSrc})
          return
        }
        var width = image.width
        var height = image.height
        var canvas = document.createElement('canvas')
        var ctx = canvas.getContext('2d')

        // set proper canvas dimensions before transform & export
        if (4 < orientation && orientation < 9) {
          canvas.width = height
          canvas.height = width
        } else {
          canvas.width = width
          canvas.height = height
        }

        // transform context before drawing image
        switch (orientation) {
          case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
          case 3: ctx.transform(-1, 0, 0, -1, width, height); break;
          case 4: ctx.transform(1, 0, 0, -1, 0, height); break;
          case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
          case 6: ctx.transform(0, 1, -1, 0, height, 0); break;
          case 7: ctx.transform(0, -1, -1, 0, height, width); break;
          case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
          default: break;
        }

        // draw image
        ctx.drawImage(image, 0, 0)
        resolve({image: canvas, src: canvas.toDataURL('image/jpeg')})
      }).catch((error) => {
        reject('error loading image in fixImageOrientation')
      })
    })
  },
  /*
  * given an url, test if it can be loaded as image
  * return Promise resolve if yes, or reject if no
   */
  isUrlImage (url, timeoutInterval) {
    return new Promise((resolve, reject) => {
      timeoutInterval = timeoutInterval || 5000
      var timer, img = new Image()
      img.onerror = img.onabort = function () {
        clearTimeout(timer)
        reject('Fail loading url as image')
      }
      img.onload = function () {
        clearTimeout(timer)
        resolve(true)
      }
      timer = setTimeout(() => {
        //reset .src to invalid URL so it stops preious
        //loading, but doesn't trigger new load
        img.src = '//!!!!!/noexist.jpg'
        reject('Timeouted loading url as image')
      }, timeoutInterval)
      img.src = url
    })
  }
}

export default Utils
