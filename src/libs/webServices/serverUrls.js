export default {
  API_SERVER: 'https://api.scopemedia.com',
  AWS_HOST: 'https://scopemedia4.s3.amazonaws.com',
  VIDEO_DEEP_SERVER: 'http://34.223.218.178:3000',
  VIDEO_NODE_SERVER: 'http://172.105.241.172:3000', // 172.104.106.32
  NODE_SERVER: 'http://vidsvc.scopephotos.com:3000'
}
