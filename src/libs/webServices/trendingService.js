'use strict'
import axios from 'axios'
import store from '@/store'
import serverUrls from '@/libs/webServices/serverUrls.js'

export default {
  getTrendings () {
    return axios({
      method: 'GET',
      url: `${serverUrls.API_SERVER}/trending/v2/trendings`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  },

  getTrendingMedias (startDate, endDate, channel, page, size) {
    return axios({
      method: 'GET',
      url: `${serverUrls.API_SERVER}/trending/v2/medias?startDate=${startDate}&endDate=${endDate}&channel=${channel}&page=${page}&size=${size}`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  },

  getTrendingGraphs (trendingId) {
    return axios({
      method: 'GET',
      url: `${serverUrls.API_SERVER}/trending/v2/trendings/${trendingId}/graphs`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  },

  searchTrendingGraphHighlightImages (graphId, page, size, query) {
    return axios({
      method: 'GET',
      url: `${serverUrls.API_SERVER}/trending/v2/trendings/coordinates?trendingGraphId=${graphId}&page=${page}&size=${size}&${query}`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  },

  searchTrendingGraphDistinctCoordinatesInPolygon (graphId, points) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/trending/v2/trendings/coordinates/distinct`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      },
      data: {
        trendingGraphId: graphId,
        points: points
      }
    })
  },

  getTrendingGraphColors (graphId) {
    return axios({
      method: 'GET',
      url: `${serverUrls.API_SERVER}/trending/v2/trendings/graphs/${graphId}/colors`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  },

  getTrendingTrends (trendingId) {
    return axios({
      method: 'GET',
      url: `${serverUrls.API_SERVER}/trending/v2/trendings/${trendingId}/trends`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  },

  // hardcode some demo user for demo purpose
  getTrendingClientSecret (clientId) {
    switch (clientId) {
      case 'aritzia':
        return 'DYtOeaKLfTkwQbihXIVw5V2lTNd6eZEYv9OEjefbsgLI1A8lxwpxt4gcZTnpnKdS'
      case 'gucci':
        return 'gJw0eA07QnOpu3wCWOt5lPFS13EbVHYvl6MdWqXC58e7bLZCyd8T6KhjMPRF1fyl'
      case 'forever21':
        return '23X8RKYfFr3OKPkS1bQKfAC6OnzzyxBWnyyuiZlwqZtAeEVj3FumyND2z5I5MPPw'
    }
  },

  getTrendingRecommendations (clientId) {
    return axios({
      method: 'GET',
      url: `${serverUrls.API_SERVER}/trending/v2/recommendations?page=0&size=1`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`,
        'Client-Id': clientId,
        'Client-Secret': this.getTrendingClientSecret(clientId)
      }
    })
  },

  getTrendingRecommendation (clientId, recommendationId) {
    return axios({
      method: 'GET',
      url: `${serverUrls.API_SERVER}/trending/v2/recommendations/${recommendationId}`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`,
        'Client-Id': clientId,
        'Client-Secret': this.getTrendingClientSecret(clientId)
      }
    })
  },

  resetTrendingRecommendation (clientId, recommendationId) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/trending/v2/recommendations/${recommendationId}/reset`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`,
        'Client-Id': clientId,
        'Client-Secret': this.getTrendingClientSecret(clientId)
      }
    })
  },

  createTrendingRecommendation (clientId, trendsUsed) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/trending/v2/recommendations`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`,
        'Client-Id': clientId,
        'Client-Secret': this.getTrendingClientSecret(clientId)
      },
      data: {
        queryTime: (new Date()).toISOString(),
        trendingsUsed: trendsUsed.join(',')
      }
    })
  },

  deleteTrendingTrend (trendId) {
    return axios({
      method: 'DELETE',
      url: `${serverUrls.API_SERVER}/trending/v2/trends/${trendId}`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  },

  deleteTrendingCluster (clusterId) {
    return axios({
      method: 'DELETE',
      url: `${serverUrls.API_SERVER}/trending/v2/trends/clusters/${clusterId}`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`
      }
    })
  }
}
