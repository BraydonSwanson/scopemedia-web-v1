'use strict'
import axios from 'axios'
import store from '@/store'
import serverUrls from '@/libs/webServices/serverUrls.js'
import credentials from '@/libs/credentials.js'

export default {
  embeddingSearch (sentence) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/embedding-search/v2/embeding-search`,
      headers: {
        'Content-Type': 'application/json',
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: { sentence }
    })
  },
  tagAttribute (base64) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/score/v2/tag-attributes`,
      headers: {
        'Content-Type': 'application/json',
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: { base64 }
    })
  },
  floorSearch (payload) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/floor/v2/floor-search`,
      headers: {
        'Content-Type': 'application/json',
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: payload
    })
  },
  gan (model, base64a, base64b) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/gan-${model}/v2/base64_gan`,
      headers: {
        'Content-Type': 'application/json',
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: {
        base64a: base64a,
        base64b: base64b
      }
    })
  },
  ganSingle (base64, dim, value) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/gan-dress/v2/base64_gan_single_image`,
      headers: {
        'Content-Type': 'application/json',
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: { base64, dim, value }
    })
  },
  ganStyleTransfer (base64) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/gan-transfer/v2/base64_style_transfer`,
      headers: {
        'Content-Type': 'application/json',
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: { base64 }
    })
  }
}
