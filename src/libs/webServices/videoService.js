'use strict'
import axios from 'axios'
import store from '@/store'
import serverUrls from '@/libs/webServices/serverUrls.js'
import credentials from '@/libs/credentials.js'

export default {
  videoSearch (id, start, end, flag, deep) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/video2/v2/search`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`,
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: {
        'id': id,
        'start': start,
        'end': end,
        'flag': flag,
        'deep': deep
      }
    })
  },

  videoDeepSearch (startTime, endTime, videoId='uploads/ad_output.mp4') {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/video-deep/v2/search`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`,
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: {
        deep: true,
        start: startTime,
        end: endTime,
        flag: false,
        id: videoId
      }
    })
  },

  fetchVideoHighlight (videoName, highlightLength, groundTruth) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/video/v2/highlight`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`,
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: {
        'videoName': videoName,
        'highlightLength': highlightLength,
        'groundTruth': groundTruth
      }
    })
  },

  getVideoAdTag (videoName) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/video/v2/adTag`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`,
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: {
        videoName: videoName
      }
    })
  },

  exportVideo (videoClips, width, height, outputName, concatenate = true) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/video/v2/concatenate${concatenate ? '' : '-separate'}`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`,
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: {
        'width': width,
        'height': height,
        'video_clips': videoClips,
        'output_name': outputName
      }
    })
  },

  exportVideo2 (videoClips, width, height, outputName) {
    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/video2/v2/concatenate`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${store.state.accessToken}`,
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: {
        'width': width,
        'height': height,
        'video_clips': videoClips,
        'output_name': outputName
      }
    })
  },
}
