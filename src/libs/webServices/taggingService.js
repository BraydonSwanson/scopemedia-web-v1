'use strict'
import axios from 'axios'
import store from '@/store'
import serverUrls from '@/libs/webServices/serverUrls.js'
import credentials from '@/libs/credentials.js'

export default {
  predictImage ({ mediaUrl, base64, area, modelId, resultSize }) {
    let data = {}
    if (mediaUrl) { data.mediaUrl = mediaUrl }
    else if (base64) { data.base64 = base64 }
    if (area) { data.area = area }
    if (modelId) { data.modelId = modelId }
    if (resultSize) { data.resultSize = resultSize }

    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/tagging/v2/prediction`,
      headers: {
        'Content-Type': 'application/json',
        'Client-Id': credentials.demo.clientId,
        'Client-Secret': credentials.demo.clientSecret
      },
      data: data
    })
  }
}
