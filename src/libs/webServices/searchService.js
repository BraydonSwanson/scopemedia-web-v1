'use strict'
import axios from 'axios'
import store from '@/store'
import serverUrls from '@/libs/webServices/serverUrls.js'

export default {
  searchSimilarImage({mediaId, mediaUrl, base64, area, modelId, clientId, clientSecret}) {
    var headers = {'Content-Type': 'application/json'}
    if (typeof clientId === 'string' && clientId.length > 0 && typeof clientSecret === 'string' && clientSecret.length > 0) {
      headers['Client-Id'] = clientId
      headers['Client-Secret'] = clientSecret
    }

    var payload = {}
    if (!isNaN(parseInt(mediaId))) {
      payload.mediaId = mediaId
    } else if (typeof mediaUrl === 'string' && mediaUrl.length > 0) {
      payload.mediaUrl = mediaUrl
    } else if (typeof base64 === 'string' && base64.length > 0) {
      payload.base64 = base64
    } else {
      throw new Error('One of mediaId, mediaUrl or base64 is required, but none are valid')
    }

    if (typeof modelId === 'string' && modelId.length > 0) {
      payload.modelId = modelId
    }

    if (area && area.hasOwnProperty('x') && area.hasOwnProperty('y') &&
        area.hasOwnProperty('w') && area.hasOwnProperty('h') &&
        !isNaN(area.x) && !isNaN(area.y) && !isNaN(area.w) && !isNaN(area.h)) {
      payload.area = area
    }

    return axios({
      method: 'POST',
      url: `${serverUrls.API_SERVER}/search/v2/similar`,
      headers: headers,
      data: payload
    })
  },

  deleteImage ({mediaId, clientId, clientSecret}) {
    var headers = {'Content-Type': 'application/json'}
    if (typeof clientId === 'string' && clientId.length > 0 && typeof clientSecret === 'string' && clientSecret.length > 0) {
      headers['Client-Id'] = clientId
      headers['Client-Secret'] = clientSecret
    }

    return axios({
      method: 'DELETE',
      url: `${serverUrls.API_SERVER}/search/v2/medias/${mediaId}`,
      headers: headers
    })
  }
}
