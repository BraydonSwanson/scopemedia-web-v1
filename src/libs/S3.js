/**
 * Created by Alex on 2017-06-28.
 */
const CryptoJS = require('crypto-js')
import accountService from '@/libs/webServices/accountService.js'
//A Amazon S3 class is used to generate needed key information and upload files onto S3 bucket.
class S3 {


  constructor (newDecryptObj) {
    this.decryptObj = newDecryptObj
    localStorage.setItem('_scopeMedia_upload_token', JSON.stringify(newDecryptObj))
  }

  /**
   * A static method for testing token expired
   * @returns {*}
   */
  static testTokenExpired () {
    if (localStorage.getItem('_scopeMedia_upload_token') !== null) {
      let curTime = new Date(),
        tokenObj = localStorage.getItem('_scopeMedia_upload_token'),
        timeStamp = JSON.parse(tokenObj).expirationDate
      if (curTime > timeStamp) {
        return this.getS3Token()
      } else {
        return new Promise((resolve) => {
          let decryptObj = JSON.parse(localStorage.getItem('_scopeMedia_upload_token'))
          resolve(decryptObj)
        })
      }
    } else {
      localStorage.removeItem('_scopeMedia_upload_token')
      return this.getS3Token()
    }
  }


  /**
   * A static method for getting S3 token from server.
   * If success:
   * @returns {Promise.resolve}
   * else
   * @returns {Promise.reject}
   */
  static getS3Token () {
    let self = this
    return new Promise((resolve, reject) => {
        accountService.getS3Token().then((res) => {
          let data = res.data.data
          let key = res.data.key
          let decryptObj = self.decryptData(data, key)
          resolve(decryptObj)
        }).catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  }

  /**
   * decrypt data.
   * @param data
   * @param key
   */
  static decryptData (data, key) {
    //Decrypt message with CryptoJS from
    //http://stackoverflow.com/questions/14958103/how-to-decrypt-message-with-cryptojs-aes-i-have-a-working-ruby-example
    let rawData = atob(data)
    let iv = btoa(rawData.substring(0, 16))
    let crypttext = btoa(rawData.substring(16))

    var plaintextArray = CryptoJS.AES.decrypt(
      {
        ciphertext: CryptoJS.enc.Base64.parse(crypttext),
        salt: ""
      },
      CryptoJS.enc.Hex.parse(key),
      { iv: CryptoJS.enc.Base64.parse(iv)});

    function hex2a(hex) {
      var str = ''
      for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16))
      return str
    }

    return JSON.parse(hex2a(plaintextArray.toString()))
  }





  // setDecryptObj(newDecryptObj){
  //   this.decryptObj = newDecryptObj;
  //   localStorage.setItem('_scopeMedia_upload_token', JSON.stringify(newDecryptObj));
  // }




  /**
   * Get S3 signature for Authorization header of HTTP request.
   * @param fileType
   * @param fileName
   * @returns {string}
   * http://docs.aws.amazon.com/AmazonS3/latest/dev/S3_Authentication2.html
   * This signing method used Temporary Security Credentials
   * http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html
   *
   * Signing template:
   * Authorization = "AWS" + " " + AWSAccessKeyId + ":" + Signature;

   * Signature = Base64( HMAC-SHA1( YourSecretAccessKeyID, UTF-8-Encoding-Of( StringToSign ) ) );

   * StringToSign = HTTP-Verb + "\n" +
   * Content-MD5 + "\n" +
   * Content-Type + "\n" +
   * Date + "\n" +
   * CanonicalizedAmzHeaders +
   * CanonicalizedResource;

   * CanonicalizedResource = [ "/" + Bucket ] +
   * <HTTP-Request-URI, from the protocol name up to the query string> +
   * [ subresource, if present. For example "?acl", "?location", "?logging", or "?torrent"];

   * CanonicalizedAmzHeaders = <described below>
   *
   */
 generateS3Signature (method, fileType, fileName, timeStr, dirName, bucketName) {
   let token = this.decryptObj
   let accessKey = token.accessKey
   let secretKey = token.secretKey
   let signStr = ""
   signStr += `${method}\n`
   signStr += `\n`
   signStr += fileType.length === 0 ? `\n`: `${fileType}\n`
   signStr += `\nx-amz-date:${timeStr}\n`
   signStr += `x-amz-security-token:${token.securityToken}\n`
   signStr += `/${bucketName}/`
   signStr += dirName.length === 0 ? `` : `${dirName}`
   signStr += fileName.length === 0 ? `` : `/${fileName}`


   //console.log('signStr:   \n' + signStr);


   let hashedSignStr = CryptoJS.HmacSHA1(signStr, secretKey)
   let encodedSignStr = CryptoJS.enc.Base64.stringify(hashedSignStr)
   let authStr = `AWS ${accessKey}:${encodedSignStr}`

   return authStr
 }


  /**
   * Upload file to s3 bucket
   * @param file
   * @param filename
   * @param url
   * @param method
   * @returns {Promise}
   */
 uploadFile (file, filename, url, method, dirName='demo', bucketName='scopemedia4') {
   let self = this
   if (!file instanceof File) {
     throw 'Parameter need to be File type'
   }
   if (!url){
     throw 'Please provide a url'
   }
   return new Promise(function(resolve, reject) {
     let request,
       httpMethod,
       token,
       curTimeStr

     token = self.decryptObj
     httpMethod = method || 'PUT'
     request = new XMLHttpRequest()
     request.open(httpMethod, url)
     curTimeStr = new Date().toGMTString()



     request.setRequestHeader("Content-Type", file.type)
     request.setRequestHeader("x-amz-security-token", token.securityToken)
     request.setRequestHeader("Authorization", self.generateS3Signature(method, file.type, filename, curTimeStr, dirName, bucketName))
     request.setRequestHeader("x-amz-date", curTimeStr)



     request.onload = function () {
       if (request.status >= 200 && request.status < 400) {
         resolve("File upload success. " , request.response)
       } else {
         reject(Error('Image didn\'t load successfully; error code:' + request.statusText))
       }
     }


     request.onerror = function () {
       reject(Error('There was a network error when upload image.'))
     }


     request.send(file)


     //Dont know why this event can not be fired.
     // request.onprogress = function(e){
     //   console.log(e);
     //   console.log('ab');
     // };
   })

 }


 deleteFile (filename, url, method, dirName, bucketName) {
   let self = this;
   return new Promise(function(resolve, reject) {
     let request,
         httpMethod,
         token,
         curTimeStr

     token = self.decryptObj
     httpMethod = method || 'DELETE'
     request = new XMLHttpRequest()
     request.open(httpMethod, url)
     curTimeStr = new Date().toGMTString()

     request.setRequestHeader("x-amz-security-token", token.securityToken)
     request.setRequestHeader("Authorization", self.generateS3Signature(method, '', filename, curTimeStr, dirName, bucketName))
     request.setRequestHeader("x-amz-date", curTimeStr)

     request.onload = function () {
       if (request.status >= 200 && request.status < 400) {
         resolve("File delete success. ",request.response);
       } else {
         reject(Error('Image didn\'t load successfully; error code:' + request.statusText))
       }
     }

     request.onerror = function () {
       reject(Error('There was a network error when delete image.'))
     }

     request.send()
   })
 }


  /**
   *
   * @param xml
   * @returns {{}}  JSON object
   * @private
   */
  _xmlToJson(xml) {
    var self = this;
    // Create the return object
    var obj = {}

    if (xml.nodeType == 1) { // element
      // do attributes
      if (xml.attributes.length > 0) {
        obj["@attributes"] = {}
        for (var j = 0; j < xml.attributes.length; j++) {
          var attribute = xml.attributes.item(j)
          obj["@attributes"][attribute.nodeName] = attribute.nodeValue
        }
      }
    } else if (xml.nodeType == 3) { // text
      obj = xml.nodeValue
    }

    // do children
    // If just one text node inside
    if (xml.hasChildNodes() && xml.childNodes.length === 1 && xml.childNodes[0].nodeType === 3) {
      obj = xml.childNodes[0].nodeValue;
    } else if (xml.hasChildNodes()) {
      for(var i = 0; i < xml.childNodes.length; i++) {
        var item = xml.childNodes.item(i)
        var nodeName = item.nodeName
        if (typeof(obj[nodeName]) == "undefined") {
          obj[nodeName] = self._xmlToJson(item)
        } else {
          if (typeof(obj[nodeName].push) == "undefined") {
            var old = obj[nodeName]
            obj[nodeName] = []
            obj[nodeName].push(old)
          }
          obj[nodeName].push(self._xmlToJson(item))
        }
      }
    }
    return obj
  }
}

export default S3
