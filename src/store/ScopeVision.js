import trendingService from '@/libs/webServices/trendingService.js'
import TrendingUtils from '@/components/dashboard/scopeVision/TrendingUtils.js'

export default {
  namespaced: true,
  state: {
    graphs: {},
    trendings: null
  },
  mutations: {
    SET_TRENDING_GRAPHS (state, graphs) {
      state.graphs[graphs.trendingId] = graphs
    },
    SET_TRENDINGS (state, trendings) {
      state.trendings = trendings
    },
    REMOVE (state) {
      state.graphs = []
      state.trendings = null
    }
  },
  actions: {
    remove ({ commit }) { commit('REMOVE') },
    getTrendingGraphs ({ commit, state }, { trendingId, callback })  {
      if (state.graphs[trendingId]) {
        callback(state.graphs[trendingId])
      } else {
        trendingService.getTrendingGraphs(trendingId)
        .then((response) => {
          commit('SET_TRENDING_GRAPHS', TrendingUtils.parseGraphs(trendingId, response.data))
          if (callback) {
            callback(state.graphs[trendingId])
          }
        })
        .catch((error) => {
          console.log(error)
        })
      }
    },
    getTrendings ({ commit, state }, callback)  {
      if (state.trendings) {
        callback(state.trendings)
      } else {
        trendingService.getTrendings().then((response) => {
          var trendingsObj = {}
          for (var i of response.data) {
            trendingsObj[i.id] = i
          }

          commit('SET_TRENDINGS', trendingsObj)

          if (callback) {
            callback(state.trendings)
          }
        }).catch((error) => {
          console.log(error)
        })
      }
    },
  }
}
