import accountService from '@/libs/webServices/accountService.js'

export default {
  namespaced: true,
  state: {
    applicationList: null
  },
  mutations: {
    SET_APPLICATION_LIST (state, { applicationList }) { state.applicationList = applicationList },
    REMOVE (state) { state.applicationList = null }
  },
  actions: {
    async getApplicationList ({ commit, state, rootGetters }, { refresh }) {
      if (state.applicationList && !refresh) { return true }
      try {
        var applicationList = await accountService.getApplicationList()
        if (rootGetters.isAdmin) {
          applicationList = applicationList.sort(function (a, b) {
            if (a.user.login > b.user.login) return 1
            if (a.user.login < b.user.login) return -1
            return 0
          })
        }
        commit('SET_APPLICATION_LIST', { applicationList })
        return true
      } catch (error) {
        console.error(error)
      }
      throw new Error('Failed loading application list')
    },
    async deleteApplication ({ dispatch }, { appId }) {
      try {
        var response = await accountService.deleteApplication(appId)
        if (response && response.status === 200) {
          await dispatch('getApplicationList', { refresh: true })
          return true
        }
      } catch (error) {
        console.error(error)
      }
      throw new Error('Failed deleting application')
    },
    async createApplication ({ dispatch }, { appName }) {
      try {
        var response = await accountService.createApplication(appName)
        if (response && response.status === 201) {
          await dispatch('getApplicationList', { refresh: true })
          return true
        }
      } catch (error) {
        console.error(error)
      }
      throw new Error('Failed creating application')
    },
    remove ({ commit }) { commit('REMOVE') }
  }
}
