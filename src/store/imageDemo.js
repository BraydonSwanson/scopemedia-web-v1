export default {
  namespaced: true,
  state: {
    imgList: [],
    queryImg: null,
    setQueryImg: {
      flag: false,
      url: '',
      mediaId: ''
    },
    viewerLoader: false
  },
  mutations: {
    SET_IMG_LIST (state, { newList }) { state.imgList = newList },
    APPEND_IMG_LIST (state, { appendList }) { state.imgList = state.imgList.concat(appendList) },
    CLEAR_IMG_LIST (state) { state.imgList = [] },
    SET_QUERY_IMG (state, { newImg }) { state.queryImg = newImg },
    CLEAR_QUERY_IMG (state) { state.queryImg = null },
    ENABLE_SET_QI (state , { newUrl, newMediaId }) {
      state.setQueryImg.flag = true
      state.setQueryImg.url = newUrl
      state.setQueryImg.mediaId = newMediaId || ''
    },
    DISABLE_SET_QI (state) { state.setQueryImg.flag = false },
    SHOW_VIEWER_LOADER (state) { state.viewerLoader = true },
    HIDE_VIEWER_LOADER (state) { state.viewerLoader = false }
  },
  actions: {
    setImgList ({ commit }, { newList }) { commit('SET_IMG_LIST', { newList }) },
    appendImgList ({ commit }, { appendList }) { commit('APPEND_IMG_LIST', { appendList }) },
    clearImgList ({ commit }) { commit('CLEAR_IMG_LIST') },
    setQueryImg ({ commit }, { newImg }) { commit('SET_QUERY_IMG', { newImg }) },
    clearQueryImg ({ commit }) { commit('CLEAR_QUERY_IMG') },
    enableSetQueryImg ({ commit }, { newUrl, newMediaId }) { commit('ENABLE_SET_QI', { newUrl, newMediaId }) },
    disableSetQueryImg ({ commit }) { commit('DISABLE_SET_QI') },
    showViewerLoader ({ commit }) { commit('SHOW_VIEWER_LOADER') },
    hideViewerLoader ({ commit }) { commit('HIDE_VIEWER_LOADER') }
  }
}
