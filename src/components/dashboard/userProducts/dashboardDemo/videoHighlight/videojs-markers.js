/**
 * Created by Alex on 2017-09-11.
 */
import videojs from 'video.js'
const Plugin = videojs.getPlugin('plugin');

export default class progressMarkers extends Plugin {
  constructor(player, options) {
    super(player, options);
    this.player = player;
    this.options = Object.assign({
      timeFrames: []
    }, options);
    if (this.options.timeFrames.length > 0) {
      this.player.on('loadedmetadata', () => {
        console.log('initialize from constructor');
        this.initialize();
      })
    }
  }

  dispose() {
    super.dispose()
    videojs.log('the progress markers plugin is being disposed')
  }

  initialize(options = {}) {
    //if the plugin is initialize on the fly.
    if (Object.keys(options).length !== 0) {
      this.options = Object.assign({}, options);
    }
    videojs.log('Initializing progress marker plugin', this.options);
    this.removeAll();
    this.addProgressMarker();
  }

  addProgressMarker() {
    const videoDuration = this.player.duration();
    const progressHolder = this.player.contentEl().querySelector('.vjs-progress-holder');
    for (let timeFrame of this.options.timeFrames) {
      const timePercent = this.calculateTimePercent(videoDuration, timeFrame);
      let markerDiv = this.createMarkerDiv(timePercent, timeFrame)
      progressHolder.appendChild(markerDiv);
    }
  }

  removeAll() {
    const progressHolder = this.player.contentEl().querySelector('.vjs-progress-holder');
    const progressMarkers = document.querySelectorAll('.vjs-progress-marker');
    for (let domNode of progressMarkers) {
      progressHolder.removeChild(domNode);
    }
  }

  calculateTimePercent(duration, timeFrame) {
    return {
      start: (timeFrame[0] / duration * 100),
      end: (timeFrame[1] / duration * 100),
      duration: ((timeFrame[1] - timeFrame[0]) / duration * 100)
    }
  }

  createMarkerDiv(timePercent, timeFrame) {
    let markerDiv = videojs.dom.createEl('div',
      {
        className: 'vjs-progress-marker'
      },
      {
        'data-marker-start': timeFrame[0],
        'data-marker-end': timeFrame[1]
      })
    markerDiv.style.left = timePercent.start.toFixed(2) + '%'
    markerDiv.style.width = timePercent.duration.toFixed() + '%'
    return markerDiv
  }
}

