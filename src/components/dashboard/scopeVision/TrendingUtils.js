const BASE_URL = 'https://s3-us-west-2.amazonaws.com/scopemedia4/trending-service';

import store from '@/store';

const graphName = {
  source: 'Overall View',
  fashion: 'Fashion Classification',
  detection: 'Element Detection'
}

export default {
  parseGraphs(trendingId, data) {
    let graphs = {
      trendingId: trendingId,
      source: null,
      fashion: null,
      detection: null,
      categories: [],
      rawData: data
    };

    for (var g of graphs.rawData) {
      if (g.type === 'TSNE' && g.source === 'Media' && g.fashionOnly === false) {
        g.name = graphName.source;
        g.slug = 'source';
        graphs.source = g;
      } else if (g.type === 'TSNE' && g.source === 'Media' && g.fashionOnly === true) {
        g.name = graphName.fashion;
        g.slug = 'fashion'
        graphs.fashion = g;
      } else if (g.type === 'TSNE' && g.source === 'MediaSection' && g.fashionOnly === true) {
        g.name = graphName.detection;
        g.slug = 'detection';
        graphs.detection = g;
      } else if (g.type === 'KMEANS') {
        g.name = `${g.category} Graph`;
        g.slug = 'category';
        graphs.categories.push(g);
      }
    }

    return graphs;
  },

  parseChannelName(channel) {
    if (channel == 'Tagboard' || channel == 'Socialmedia') {
      return 'Social Media';
    }
    return channel;
  },

  filterImages(coordinate, selection, radius) {
    return coordinate.filter((p) => {
      if (radius) {
        // +64 to get the center of that image
        return Math.pow(selection.x - (parseInt(p.x) + 64), 2) + Math.pow(selection.y - (parseInt(p.y) + 64), 2) <= Math.pow(radius + 64, 2);
      } else {
        return selection.x === parseInt(p.x) && selection.y === parseInt(p.y);
      }
    }).map((p) => {return p.image.indexOf('http') >= 0 ? p.image : `${BASE_URL}/cropped/${p.image}`;});
  },

  getGraphUrl(trendingId, graph) {
    return `${BASE_URL}/trendings/${trendingId}/${graph.id}_${graph.type}/cut_Image`;
  },

  getCSVUrl(trendingId, graph) {
    return `${BASE_URL}/trendings/${trendingId}/${graph.id}_${graph.type}/coordinate.csv`;
  },

  getClusterUrl(trendingId, graph) {
    return `${BASE_URL}/trendings/${trendingId}/${graph.id}_${graph.type}/cluster.csv`;
  },

  getThumbUrl(trendingId, graph) {
    return `${this.getGraphUrl(trendingId, graph)}/0/0/0.jpg`;
  },

  getAnalysisList(trendingId, graphs) {
    return [
      {
        url: `/dashboard/products/ScopeVision/${trendingId}/source`,
        thumb: require('@/assets/ScopeVision/trending/source2.png'),
        label: 'Source Images'
      },
      {
        url: `/dashboard/products/ScopeVision/${trendingId}/graph/${graphs.source.id}`,
        thumb: this.getThumbUrl(trendingId, graphs.source),
        label: graphName.source
      },
      {
        url: `/dashboard/products/ScopeVision/${trendingId}/graph/${graphs.fashion.id}`,
        thumb: this.getThumbUrl(trendingId, graphs.fashion),
        label: graphName.fashion
      },
      {
        url: `/dashboard/products/ScopeVision/${trendingId}/graph/${graphs.detection.id}`,
        thumb: this.getThumbUrl(trendingId, graphs.detection),
        label: graphName.detection
      },
      {
        url: `/dashboard/products/ScopeVision/${trendingId}/trends`,
        thumb: require('@/assets/ScopeVision/trending/trends2.png'),
        label: `${this.parseChannelName(graphs.source.channel)} Trends`
      },
      {
        url: `/dashboard/products/ScopeVision/${trendingId}/recommendation`,
        thumb: require('@/assets/ScopeVision/trending/recommendation2.png'),
        label: 'Recommend for Store',
        slug: 'recommend'
      },
      {
        url: `/dashboard/products/ScopeVision/${trendingId}/matching`,
        thumb: require('@/assets/ScopeVision/trending/matching2.png'),
        label: 'Matching'
      }
    ];
  },

  parseColors(colors) {
    var data = colors.map((c) => {
      return c.count;
    });
    var colors = colors.map((c) => {
      // this is to convert "float,float,float" to "int,int,int" because rgb is int only. Once the API chanegs to return "int,int,int", remove the spliting code
      return `rgb(${c.color.split(',').map(c => c.split('.')[0]).join(',')})`;
    });

    return {data, colors};
  }
}
