class Map {
  constructor () {
    this.init({})
  }

  init ({url = '', maxZoomLevel = 0, tileSize = 256, thumbSize = 128}) {
    this.domNode = {
      width: 1,
      height: 1
    }
    this.physical = {
      mapSize: Math.pow(2, maxZoomLevel) * tileSize,
      thumbSize: thumbSize,
      tileSize: tileSize
    }
    this.graphing = {
      thumbSize: this.toGraphing(this.physical.thumbSize)
    }
    this.url = url
    this.maxZoomLevel = maxZoomLevel
    this.zoomState = {x: 0, y: 0, k: 1}
    this.scaling = false

    // params which have dynamic drawing size relatively to zoom scale
    this.dynamicParams = {
      highlightCircleRadius: () => Math.min(this.domNode.width, this.domNode.height) / 6 / this.zoomState.k,
      polygonMarkingSize: () => 4 / this.zoomState.k
    }
  }

  updateZoomState (newZoomState) {
    var result = {scalingstop: false}

    if (this.zoomState.k !== newZoomState.k) {
      this.scaling = true
    } else {
      if (this.scaling) {
        result.scalingstop = true
      }
      this.scaling = false
    }

    this.zoomState = newZoomState

    return result
  }

  toPhysical (v) {
    return Math.round(v / this.physical.tileSize * this.physical.mapSize)
  }

  toGraphing (v) {
    return v / this.physical.mapSize * this.physical.tileSize
  }
}

const map = new Map()
export default map
